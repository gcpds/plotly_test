import datetime
import time
import logging

from scipy.signal import resample

import dash
import dash_core_components as dcc
import dash_html_components as html
from plotly import graph_objs as go
import random

import plotly
from dash.dependencies import Input, Output, State

from openbci.stream.ws import CythonWs

import numpy as np

########################################################################
class Line:
    """"""

    #----------------------------------------------------------------------
    def __init__(self, stream, window=30, channels=16, interval=1000, points=900):
        """Constructor"""
        self.stream = stream
    
        self.channels = channels  # Channels
        self.window = window  # Window
        self.points = points  # Width self.data
        self.interval = interval  # interval

        self.optimal_K = int(self.points / (self.window / (self.interval / 1000)))

        self.app = dash.Dash(__name__)
        self.app.layout = html.Div(
            html.Div([
                dcc.Graph(id='live_update_graph',
        
                          config={
                              'displayModeBar': False,
                          },
        
                          ),
        
                dcc.Interval(
                    id='interval-component',
                    interval=self.interval,  # in milliseconds
                    n_intervals=0
                ),
        
        
        
            ])
        )

        
        self.data = np.ones((self.channels, self.points)) * np.arange(0, self.channels).reshape(self.channels, 1)
        
        
        time_data = np.linspace(-self.window, 0, self.data.shape[1])
        c = time_data[1] - time_data[0]
        
        
        self.fig = plotly.subplots.make_subplots(rows=1, cols=1)
        
        
        for i, d in enumerate(self.data):
        
            self.fig.append_trace({
                'x': time_data.tolist(),
                'y': d.tolist(),
                'name': f'Cahnnel-{i+1}',
                'mode': 'lines',
                'type': 'scatter',
                'line': {'width': 1},
            }, 1, 1)
        
        
        self.fig.layout.update(showlegend=False,
                          template='plotly_dark',
                          width=1200, height=700,
                          )
        
        self.fig.layout.xaxis.update(title='Time [s]',
                                range=[-self.window, 0],
                                autorange=False,
        
                                )
        
        
        self.fig.layout.yaxis.ticktext = [f'Channel-{k}' for k in range(self.data.shape[0])]
        self.fig.layout.yaxis.tickvals = list(range(16))
        
        # t0 = time.time()
        
        
        # self.app.callback(Output('live_update_graph', 'figure'), [Input('interval-component', 'n_intervals')])(self.callback)
        
        
        openbci = CythonWs(ip='192.168.46.66', port='8845', queue=self.stream, pack_size=1000, boardmode='BOARD_MODE_DEFAULT')
        # openbci.command(openbci.SAMPLE_RATE_16KHZ)

        

    
    # ----------------------------------------------------------------------
    # @app.callback(Output('live_update_graph', 'figure'), [Input('interval-component', 'n_intervals')])
    def callback(self, n):
        
        if self.stream.qsize():
            
            
            while  self.stream.qsize():
                
                eeg, aux, timestamp = self.stream.get()
                index = list([int(d) for d in np.linspace(0, eeg.shape[1]-1, self.optimal_K)])
                eeg = eeg[:,index]
            
                self.data = np.roll(self.data, -self.optimal_K)
                self.data[:, -self.optimal_K:] = eeg
            
                for i, d in enumerate(self.fig.data):
                    d.y = i + self.data[i]
    
        return self.fig


    #----------------------------------------------------------------------
    def run(self):
        """"""
        self.app.run_server(debug=True)
