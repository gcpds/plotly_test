import time
from eeg_plotly import Line
from queue import Queue
import threading

# from openbci.acquisition import CythonWiFi as Device
# openbci = Device(ip_address='192.168.1.185')

from openbci.stream.ws import CythonWs

INTERVAL = 1000


line = Line(Queue(), interval=INTERVAL)
line.run()



# openbci = CythonWs(ip='192.168.46.66', port='8845', queue=my_queue, pack_size=INTERVAL, boardmode='BOARD_MODE_DEFAULT')
# openbci.command(openbci.SAMPLE_RATE_16KHZ)
