import datetime
import time
import logging

import dash
import dash_core_components as dcc
import dash_html_components as html
from plotly import graph_objs as go
import random

import plotly
from dash.dependencies import Input, Output, State


import numpy as np


H = 16  # Channels
T = 30  # Window
M = 900  # Width data
Q = 1000  # interval

K = int(900 / (T / (Q / 1000)))

app = dash.Dash(__name__)
app.layout = html.Div(
    html.Div([
        dcc.Graph(id='live_update_graph',

                  config={
                      'displayModeBar': False,
                  },

                  ),

        dcc.Interval(
            id='interval-component',
            interval=Q,  # in milliseconds
            n_intervals=0
        ),



    ])
)


data = np.ones((H, M)) * np.arange(0, H).reshape(H, 1)


time_data = np.linspace(-T, 0, data.shape[1])
c = time_data[1] - time_data[0]


fig = plotly.tools.make_subplots(rows=1, cols=1)


for i, d in enumerate(data):

    fig.append_trace({
        'x': time_data.tolist(),
        'y': d.tolist(),
        'name': f'Cahnnel-{i+1}',
        'mode': 'lines',
        'type': 'scatter',
        'line': {'width': 1},
    }, 1, 1)


fig.layout.update(showlegend=False,
                  template='plotly_dark',
                  width=1200, height=700,
                  )

fig.layout.xaxis.update(title='Time [s]',
                        range=[-T, 0],
                        autorange=False,
                        )


fig.layout.yaxis.ticktext = [f'Channel-{k}' for k in range(data.shape[0])]
fig.layout.yaxis.tickvals = list(range(16))


# ----------------------------------------------------------------------
@app.callback(Output('live_update_graph', 'figure'), [Input('interval-component', 'n_intervals')])
def callback(n):
    global data, t0

    data = np.roll(data, -K)
    data[:, -K:] = np.random.random((data.shape[0], K)) + np.arange(0, data.shape[0]).reshape(data.shape[0], 1)

    for i, d in enumerate(fig.data):
        d.y = data[i]

    return fig


if __name__ == '__main__':
    app.run_server(debug=True)
