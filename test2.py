import dash
from dash.dependencies import Input, Output, State
import dash_html_components as html
import dash_core_components as dcc
import random

app = dash.Dash(__name__)

app.scripts.config.serve_locally = True
app.css.config.serve_locally = True

app.layout = html.Div([

    html.Div([
        html.H3('Extend multiple traces at once'),
        dcc.Graph(
            id='graph-extendable-2',
            animate=True,
            animation_options={

                'transition': {
                    'duration': 1000,
                    'easing': 'linear',
                },
                'frame': {
                    'duration': 1000,
                    # # 'redraw': True,
                    # 'easing': 'linear',
                },
                'mode': 'afterall',
                'hide': False,
            },
            figure=dict(
                data=[{'x': [0, 1, 2, 3, 4],
                       'y': [0, .5, 1, .5, 0],
                       'mode':'lines',
                       'type': 'scatter',
                       },
                      {'x': [0, 1, 2, 3, 4],
                       'y': [0, .5, 1, .5, 0],
                       'mode':'lines',
                       'type': 'scatter',
                       },
                      {'x': [0, 1, 2, 3, 4],
                       'y': [0, .5, 1, .5, 0],
                       'mode':'lines',
                       'type': 'scatter',
                       },

                      ],
            )
        ),
    ]),
    dcc.Interval(
        id='interval-graph-update',
        interval=1000,
        n_intervals=0),
])


@app.callback(Output('graph-extendable-2', 'extendData'),
              [Input('interval-graph-update', 'n_intervals')],
              [State('graph-extendable-2', 'figure')])
def update_extend_traces_simult(n_intervals, existing):
    """"""
    return ({
        'x': [
            [existing['data'][0]['x'][-1] + 1],
            [existing['data'][1]['x'][-1] + 1],
            [existing['data'][2]['x'][-1] + 1]
        ],
        'y': [
            [random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random()],
            [random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random()],
            [random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random(), random.random()],
        ]},
        [0, 1, 2]
    )


if __name__ == '__main__':
    app.run_server(debug=True, port=8070)
